import { call } from 'redux-saga/effects';
import configureStoreBoilerplate from './configure-store-boilerplate';
import userReducer, { createUserSaga, getUsersSaga, updateUserSaga, deleteUserSaga } from '../modules/user';

export const configureStore = (history, initialState = {}) =>
  configureStoreBoilerplate(
    {
      users: userReducer,
    },
    [
      call(createUserSaga),
      call(getUsersSaga),
      call(updateUserSaga),
      call(deleteUserSaga),
    ],
    history,
    initialState
  );
