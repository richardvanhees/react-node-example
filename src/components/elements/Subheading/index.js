import React from 'react';
import PropTypes from 'prop-types';

const Subheading = ({ children }) => (
  <h2 className="subheading">
    {children}
  </h2>
);

Subheading.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Subheading;
