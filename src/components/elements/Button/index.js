import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Button extends Component {
  static propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]).isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    style: PropTypes.object,
  };
  static defaultProps = {
    className: '',
  };

  render() {
    const { onClick, className, label } = this.props;

    return (
      <button
        className={`button ${className}`}
        onClick={onClick}
      >
        {label}
      </button>
    );
  }
}
