import React from 'react';
import PropTypes from 'prop-types';

const Table = ({ headerLabels, rows, footerRow, options, className }) => (
  <div className={`table-container ${className}`}>
    <table className="table">
      <thead>
      <tr className="table__row table__row--header">
        {headerLabels.map((label, i) => (
          <th
            className={`table__cell ${
              options.alignRight && options.alignRight.includes(i + 1) ? 'table__cell--right' : ''
              }`}
            key={`table-header-cell-${i}`}
          >
            {label}
          </th>
        ))}
      </tr>
      </thead>

      <tbody>
      {rows.map((row, i) => (
        <tr className="table__row" key={`table-row-${i}`}>
          {row.map((cell, n) => (
            <td
              className={`table__cell ${
                options.alignRight && options.alignRight.includes(n + 1) ? 'table__cell--right' : ''
                }`}
              key={`table-cell-${i}-${n}`}
            >
              {cell}
            </td>
          ))}
        </tr>
      ))}

      {footerRow && (
        <tr className="table__row table__row--footer">
          {footerRow.map((cell, i) => (
            <td
              className={`table__cell ${
                options.alignRight && options.alignRight.includes(i + 1) ? 'table__cell--right' : ''
                }`}
              key={`table-footer-${i}`}
            >
              {cell}
            </td>
          ))}
        </tr>
      )}
      </tbody>
    </table>
  </div>
);

Table.propTypes = {
  rows: PropTypes.array.isRequired,
  headerLabels: PropTypes.array.isRequired,
  footerRow: PropTypes.array,
  options: PropTypes.object,
  className: PropTypes.string,
};

Table.defaultProps = {
  options: {},
};

export default Table;
