import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ wrapperClassName, labelClassName, inputClassName, onChange, label, value, type, defaultValue, }) => (
  <div className={`input-wrapper ${wrapperClassName}`}>
    {label && (
      <p className={`input-wrapper__label ${labelClassName}`}>{label}</p>
    )}

    <input
      className={`input-wrapper__input ${inputClassName}`}
      onChange={onChange}
      value={value}
      defaultValue={defaultValue}
      type={type}
    />
  </div>
);

Input.propTypes = {
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.number,
  ]),
  wrapperClassName: PropTypes.string,
  labelClassName: PropTypes.string,
  inputClassName: PropTypes.string,
  defaultValue: PropTypes.string,
};

Input.defaultProps = {
  inputClassName: '',
  labelClassName: '',
  wrapperClassName: '',
  type: 'text',
};

export default Input;
