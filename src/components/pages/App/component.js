import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Heading from '../../elements/Heading';
import CreateNewUserBlock from './CreateNewUserBlock';
import UserOverviewBlock from './UserOverviewBlock';

export default class App extends PureComponent {
  static propTypes = {
    getUsers: PropTypes.func.isRequired,
  };

  componentDidMount = () => {
    this.props.getUsers();
  };

  render() {
    return (
      <div className="app">
        <Heading>ParkIT Test</Heading>

        <CreateNewUserBlock {...this.props} />
        <UserOverviewBlock {...this.props} />
      </div>
    );
  }
}