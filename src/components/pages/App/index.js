import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AppComponent from './component';
import { createUser, getUsers, updateUser, deleteUser } from '../../../modules/user/actions';

const mapStateToProps = ({ users }) => ({
  users: users.all,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createUser,
      getUsers,
      updateUser,
      deleteUser,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent)
