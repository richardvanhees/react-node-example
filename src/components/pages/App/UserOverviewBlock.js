import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Subheading from '../../elements/Subheading';
import Table from '../../elements/Table';
import Input from '../../elements/Input';

export default class UserOverviewBlock extends Component {
  static propTypes = {
    users: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      userBeingEdited: null,
      editedDetails: {}
    };
  }

  updateUser = async () => {
    await this.props.updateUser(this.state.userBeingEdited, this.state.editedDetails);
    await this.props.getUsers();
    await this.setState({
      userBeingEdited: null,
      editedDetails: {},
    })
  };

  setValue = (e, field) => {
    this.setState({
      editedDetails: {
        ...this.state.editedDetails,
        [field]: e.target.value,
      }
    });
  };

  deleteUser = async id => {
    await this.props.deleteUser(id);
    await this.props.getUsers();
  };

  render = () => {
    const { users } = this.props;
    const { userBeingEdited } = this.state;

    return (
      <div className="user-overview-block">
        <Subheading>User overview</Subheading>

        <Table
          headerLabels={['Given name', 'Family name', 'E-mail', '', '']}
          rows={users.map(user =>
            [
              userBeingEdited === user._id ? <Input defaultValue={user.givenName} onChange={e => this.setValue(e, 'givenName')} /> : user.givenName,
              userBeingEdited === user._id ? <Input defaultValue={user.familyName} onChange={e => this.setValue(e, 'familyName')} /> : user.familyName,
              userBeingEdited === user._id ? <Input defaultValue={user.email} onChange={e => this.setValue(e, 'email')} /> : user.email,
              userBeingEdited === user._id ? <div onClick={this.updateUser} className="user-over-block__button">Save</div> : <div onClick={() => this.setState({ userBeingEdited: user._id })} className="user-over-block__button">Edit</div>,
              <div onClick={() => this.deleteUser(user._id)} className="user-over-block__button">Delete</div>,
            ]
          )}
        />
      </div>
    );
  };
}

