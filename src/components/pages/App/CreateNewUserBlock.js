import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Input from '../../elements/Input';
import Button from '../../elements/Button';
import Subheading from '../../elements/Subheading';

export default class CreateNewUserBlock extends Component {
  static propTypes = {
    createUser: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      givenName: '',
      familyName: '',
      email: '',
    };
  }

  create = async () => {
    await this.props.createUser(this.state);
    await this.props.getUsers();
    await this.setState({
      givenName: '',
      familyName: '',
      email: '',
    });
  };

  render = () => {
    return (
      <div className="create-new-user-block">
        <Subheading>Create new user</Subheading>
        <Input
          label={"Given name"}
          value={this.state.givenName}
          onChange={e => this.setState({ givenName: e.target.value })}
        />
        <Input
          label={"Family name"}
          value={this.state.familyName}
          onChange={e => this.setState({ familyName: e.target.value })}
        />
        <Input
          label={"E-mail address"}
          value={this.state.email}
          onChange={e => this.setState({ email: e.target.value })}
        />
        <Button
          onClick={this.create}
          label={'Create user'}
          className="create-new-user-block__button"
        />
      </div>
    );
  };
}

