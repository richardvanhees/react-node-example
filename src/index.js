import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/common/index.scss';
import App from './components/pages/App';
import * as serviceWorker from './serviceWorker';
import createHistory from 'history/createBrowserHistory';
import { configureStore } from './store/configure-store';

const history = createHistory();
const store = configureStore(history);

ReactDOM.render(<App store={store} history={history} />, document.getElementById('root'));

serviceWorker.unregister();
