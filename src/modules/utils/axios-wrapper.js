import request from 'axios';

const requestWrapper = (method, { route, data, headers }) => {
  const baseUrl = `http://localhost:3001/api/v1`;
  const url = baseUrl + route;

  return request({
    method,
    url,
    data,
    json: true,
    headers: {
      ...headers,
      'content-type': 'application/json',
      Accept: 'application/json',
      'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
    },
  });
};

export const get = data =>
  typeof data === 'string'
    ? requestWrapper('get', { route: data })
    : requestWrapper('get', data);
export const post = data => requestWrapper('post', data);
export const deleteRequest = data => requestWrapper('delete', data);
export const putRequest = data => requestWrapper('put', data);
