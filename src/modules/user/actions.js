import userTypes from './types';

// Create user
export const createUserStart = () => ({
  type: userTypes.CREATE_USER_START,
});

export const createUser = data => ({
  type: userTypes.CREATE_USER,
  data,
});

export const createUserSuccess = () => ({
  type: userTypes.CREATE_USER_SUCCESS,
});

export const createUserFail = err => ({
  type: userTypes.CREATE_USER,
  err,
});

// Get user

export const getUsersStart = () => ({
  type: userTypes.GET_USERS_START,
});

export const getUsers = filters => ({
  type: userTypes.GET_USERS,
  filters,
});

export const getUsersSuccess = users => ({
  type: userTypes.GET_USERS_SUCCESS,
  users,
});


export const getUsersFail = err => ({
  type: userTypes.GET_USERS_FAIL,
  err,
});


// Update user
export const updateUserStart = () => ({
  type: userTypes.UPDATE_USER_START,
});

export const updateUser = (id, data) => ({
  type: userTypes.UPDATE_USER,
  id,
  data,
});

export const updateUserSuccess = () => ({
  type: userTypes.UPDATE_USER_SUCCESS,
});

export const updateUserFail = err => ({
  type: userTypes.UPDATE_USER_FAIL,
  err,
});

// Delete user
export const deleteUserStart = () => ({
  type: userTypes.DELETE_USER_START,
});

export const deleteUser = id => ({
  type: userTypes.DELETE_USER,
  id,
});

export const deleteUserSuccess = () => ({
  type: userTypes.DELETE_USER_SUCCESS,
});

export const deleteUserFail = err => ({
  type: userTypes.DELETE_USER_FAIL,
  err,
});
