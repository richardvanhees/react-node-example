import { call, put, takeLatest } from 'redux-saga/effects';
import { post } from '../../utils/axios-wrapper';
import userTypes from '../types';
import { createUserSuccess, createUserFail } from '../actions';

export const callToCreateUser = data =>
  post({ route: '/user', data });

export function* createUser({ data = {} }) {
  try {
    const { givenName, familyName, email } = data;

    yield call(callToCreateUser, { givenName, familyName, email });
    yield put(createUserSuccess());
  } catch (error) {
    yield put(createUserFail(error));
    console.log(error);
  }
}

export default function* createUserSaga() {
  yield takeLatest(userTypes.CREATE_USER, createUser);
}
