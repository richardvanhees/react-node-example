import { call, put, takeLatest } from 'redux-saga/effects';
import { get } from '../../utils/axios-wrapper';
import userTypes from '../types';
import { getUsersSuccess, getUsersFail } from '../actions';

export const callToGetUsers = () => get('/user');

export function* getUsers() {
  try {
    const users = yield call(callToGetUsers);
    yield put(getUsersSuccess(users.data));
  } catch (error) {
    yield put(getUsersFail(error));
    console.log(error);
  }
}

export default function* getUsersSaga() {
  yield takeLatest(userTypes.GET_USERS, getUsers);
}
