import { call, put, takeLatest } from 'redux-saga/effects';
import { deleteRequest } from '../../utils/axios-wrapper';
import userTypes from '../types';
import { deleteUserSuccess, deleteUserFail } from '../actions';

export const callToDeleteUser = data =>
  deleteRequest({ route: '/user', data });

export function* deleteUser({ id }) {
  try {
    yield call(callToDeleteUser, { id });
    yield put(deleteUserSuccess());
  } catch (error) {
    yield put(deleteUserFail(error));
    console.log(error);
  }
}

export default function* deleteUserSaga() {
  yield takeLatest(userTypes.DELETE_USER, deleteUser);
}
