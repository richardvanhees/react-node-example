import { call, put, takeLatest } from 'redux-saga/effects';
import { putRequest } from '../../utils/axios-wrapper';
import userTypes from '../types';
import { updateUserSuccess } from '../actions';

export const callToUpdateUser = data =>
  putRequest({ route: '/user', data });

export function* updateUser({ id, data }) {
  try {
    yield call(callToUpdateUser, { id, data});
    yield put(updateUserSuccess());
  } catch (error) {
    console.log(error);
  }
}

export default function* updateUserSaga() {
  yield takeLatest(userTypes.UPDATE_USER, updateUser);
}
