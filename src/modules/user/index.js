import userReducer from './reducer';
import createUserSaga from './sagas/create-user';
import getUsersSaga from './sagas/get-users';
import updateUserSaga from './sagas/update-user';
import deleteUserSaga from './sagas/delete-user';
import * as actions from './actions';

export default userReducer;
export { createUserSaga, getUsersSaga, updateUserSaga, deleteUserSaga, actions };
