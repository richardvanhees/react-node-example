import userTypes from './types';

const initialState = {
  name: null,
  phone: null,
  contactTime: null,
  adviceArea: null,
  all: [],
};

export const createUserStart = state => ({
  ...state,
  isLoading: true,
});

export const createUserSuccess = state => ({
  ...state,
  isLoading: false,
});

export const createUserFail = state => ({
  ...state,
  isLoading: false,
});

export const getUsersStart = state => ({
  ...state,
  isLoading: true,
});

export const getUsersSuccess = (state, { users }) => ({
  ...state,
  isLoading: false,
  all: users,
});

export const getUsersFail = state => ({
  ...state,
  isLoading: false,
});

export const updateUserStart = state => ({
  ...state,
  isLoading: true,
});

export const updateUserSuccess = state => ({
  ...state,
  isLoading: false,
});

export const updateUserFail = state => ({
  ...state,
  isLoading: false,
});

export const deleteUserStart = state => ({
  ...state,
  isLoading: true,
});

export const deleteUserSuccess = state => ({
  ...state,
  isLoading: false,
});

export const deleteUserFail = state => ({
  ...state,
  isLoading: false,
});

const reducer = {
  [userTypes.CREATE_USER_START]: createUserStart,
  [userTypes.CREATE_USER_SUCCESS]: createUserSuccess,
  [userTypes.CREATE_USER_FAIL]: createUserFail,
  [userTypes.GET_USERS_START]: getUsersStart,
  [userTypes.GET_USERS_SUCCESS]: getUsersSuccess,
  [userTypes.GET_USERS_FAIL]: getUsersFail,
  [userTypes.UPDATE_USER_START]: updateUserStart,
  [userTypes.UPDATE_USER_SUCCESS]: updateUserSuccess,
  [userTypes.UPDATE_USER_FAIL]: updateUserFail,
  [userTypes.DELETE_USER_START]: deleteUserStart,
  [userTypes.DELETE_USER_SUCCESS]: deleteUserSuccess,
  [userTypes.DELETE_USER_FAIL]: deleteUserFail,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
