const includedModules = [
  'user',
];

module.exports = app =>
  includedModules.forEach(route =>
    app.use('/api/v1', require(`../modules/${route}`))
  );
