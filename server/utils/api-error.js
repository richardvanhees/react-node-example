class ApiError extends Error {
  constructor(m, statusCode = 500) {
    super(m);
    this.statusCode = statusCode;
  }
}

module.exports = (message, statusCode) =>
  new ApiError(message, statusCode);