module.exports = (originalObject, keysToTransform) => {
  const transformedObject = {};

  Object.keys(originalObject).forEach(originalKey => {
    transformedObject[keysToTransform[originalKey] || originalKey] = originalObject[originalKey];
  });

  return transformedObject;
};