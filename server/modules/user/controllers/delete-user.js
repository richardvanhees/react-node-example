const apiError = require('../../../utils/api-error');
const { userModel } = require('../models/user-model');

module.exports = async (req, res, next) => {
  try {
    const userData = await userModel.findOneAndRemove({ _id: req.body.id });

    return res.status(200).json(`User ${userData.givenName} ${userData.familyName} has been successfully deleted`);
  } catch (err) {
    next(apiError(err, 500))
  }
};
