module.exports = {
  getUser: require('./get-user'),
  createUser: require('./create-user'),
  updateUser: require('./update-user'),
  deleteUser: require('./delete-user'),
};
