const apiError = require('../../../utils/api-error');
const { userModel } = require('../models/user-model');

module.exports = async (req, res, next) => {
  try {
    const userData = await userModel.findOneAndUpdate(
      { _id: req.body.id },
      req.body.data,
      { new: true }
    );

    return res.status(200).json(userData);
  } catch (err) {
    next(apiError(err, 500))
  }
};
