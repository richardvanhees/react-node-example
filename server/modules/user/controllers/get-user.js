const apiError = require('../../../utils/api-error');
const keyReplacer = require('../../../utils/key-replacer');
const { userModel } = require('../models/user-model');

module.exports = async (req, res, next) => {
  try {
    const searchCriteria = keyReplacer(req.query, { id: "_id" });
    const userData = await userModel.find(searchCriteria); // eslint-disable-line

    return res.status(201).json(userData);
  } catch (err) {
    next(apiError(err, 500))
  }
};
