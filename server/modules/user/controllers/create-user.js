const apiError = require('../../../utils/api-error');
const userCreator = require('../models/user-creator');

module.exports = async (req, res, next) => {
  try {
    await userCreator(req.body); // eslint-disable-line
    return res.status(201).json({
      success: true,
      status: 201,
      message: `User ${req.body.givenName} ${req.body.familyName} has been created`,
    });
  } catch (err) {
    next(apiError(err, 400))
  }
};
