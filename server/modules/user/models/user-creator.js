const { userModel: User } = require('./user-model');

module.exports = payload => User.create(payload);
