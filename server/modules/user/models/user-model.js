require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const userSchema = new mongoose.Schema({
  givenName: { type: String, required: true },
  familyName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
}, {
  timestamps: true,
});

module.exports = {
  userModel: mongoose.model('User', userSchema, 'users'),
};
