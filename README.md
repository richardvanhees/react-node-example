### Intro

This is an example project built with React and NodeJS.

#### Setup
- Start your mongo server with `mongod`
- Run `yarn` to install dependencies
- Run `yarn start-server`  
- Run `yarn start` in a separate terminal instance


#### Docs
API docs can be found at the following link:  
https://documenter.getpostman.com/view/3077379/RzfasCbK

#### Team

- Richard van Hees - *developer* 